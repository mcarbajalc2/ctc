<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title>CACAO TRADING COMPANY</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
</head>
<body>
	<header>
		<nav class="container">
			<div class="d-flex">
				<a class="d-block home-link" href="#">
					Cacao Trading Company
				</a>
				<ul class="list-unstyled d-flex m-0 mr-0 ml-auto">
					<li class="active">
						<a class="d-block" href="#">Inicio</a>
						<label class="active-decoration"></label>
					</li>
					<li>
						<a class="d-block" href="#">Nosotros</a>
						<label class="active-decoration"></label>
					</li>
					<li>
						<a class="d-block" href="#">Productos</a>
						<label class="active-decoration"></label>
					</li>
					<li>
						<a class="d-block" href="#">Certificaciones</a>
						<label class="active-decoration"></label>
					</li>
					<li>
						<a class="d-block" href="#">Item</a>
						<label class="active-decoration"></label>
					</li>
				</ul>
			</div>
		</nav>
	</header>
	<section>
		<div class="mc2-ly mc2-ly1">
			<div class="caption text-center">
				<h1>CACAO</h1>
				<h5>TRADING COMPANY</h5>
			</div>
			<img src="">
		</div>
		<div class="mc2-ly mc2-ly2">
			<div class="container">
				<div class="row">
					<div class="col-12 col-md-4">
						<div class="d-flex flex-column align-items-center text-center">
							<img src="{{ asset('images/icono1.png') }}">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam unde vero, odio expedita quaerat ipsam facilis iusto possimus adipisci fugit.</p>
						</div>
					</div>
					<div class="col-12 col-md-4">
						<div class="d-flex flex-column align-items-center text-center">
							<img src="{{ asset('images/icono2.png') }}">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia ipsam quae enim adipisci reiciendis architecto, esse eius laudantium similique corrupti.</p>
						</div>
					</div>
					<div class="col-12 col-md-4">
						<div class="d-flex flex-column align-items-center text-center">
							<img src="{{ asset('images/icono3.png') }}">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo, amet, alias. Sed vitae delectus consectetur commodi alias aliquam ut nam.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="mc2-ly mc2-ly3">
			<div class="container">
				<div class="d-flex flex-column">
					<div class="mc2-ly-header text-center">
						<h3>PRODUCTOS</h3>
						<div class="mc2-tab-buttons">
							<label tab-target="pane1" tab-domain="products" class="btn btn-primary active">Cacao en grano</label>
							<label tab-target="pane2" tab-domain="products" class="btn btn-primary">Cafe verde</label>
						</div>
					</div>
					<div class="mc-ly-content">
						<div class="mc2-pane active" id="pane1" tab-domain="products">
							<div class="row">
								<div class="col-12 col-md-6 d-flex align-items-center">
									<p class="pr-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum culpa sit sint omnis deserunt consequatur ex voluptas numquam fugit perspiciatis eligendi veritatis quo labore quisquam voluptate, tempore, officiis dolores. Soluta id error autem necessitatibus voluptate quidem odit reiciendis temporibus sunt? Suscipit ducimus impedit facilis vitae nulla itaque maiores harum quis.</p>
								</div>
								<div class="col-12 col-md-6 d-flex justify-content-center">
									<img src="{{ asset('images/cafe_verde.png') }}">
								</div>
							</div>
						</div>
						<div class="mc2-pane" id="pane2" tab-domain="products">
							<div class="row">
								<div class="col-12 col-md-6 d-flex align-items-center">
									<p class="pr-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quasi nobis eum sapiente necessitatibus harum beatae animi inventore sequi. Quasi magni voluptates neque consequatur a cumque.</p>
								</div>
								<div class="col-12 col-md-6 d-flex justify-content-center">
									<img src="{{ asset('images/cafe_verde.png') }}">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
                <div class="mc2-ly mc2-ly4">
                    <div class="container">
                        <div class="d-flex flex-column text-center align-items-center">
                            <h3 class="mb-4">CERTIFICACIONES</h3>
                            <p class="mb-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur, eaque, dolores, cum, porro sint quaerat voluptate a enim aliquam deleniti vero doloribus dolore ullam laborum nam aut ducimus repellendus quibusdam.</p>
                            <div class="logos-container row w-100">
                                <div class="col-6 col-md-3 d-flex justify-content-center"><img src="{{ asset('images/fair-trade-422.png') }}" alt=""></div>
                                <div class="col-6 col-md-3 d-flex justify-content-center"><img src="{{ asset('images/kosher_.jpg') }}" alt=""></div>
                                <div class="col-6 col-md-3 d-flex justify-content-center"><img src="{{ asset('images/jas-organic.png') }}" alt=""></div>
                                <div class="col-6 col-md-3 d-flex justify-content-center"><img src="{{ asset('images/USDA-Organic-01.png') }}" alt=""></div>
                            </div>
                        </div>
                    </div>
                </div>
	</section>
        <footer>
            <div class="mc2-ly fotter1">
                <div class="container">
                    <p><small>Todos los derechos reservados &copy; 2018 - Cacao Trading Company</small></p>
                </div>
            </div>
        </footer>
	<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
</body>
</html>