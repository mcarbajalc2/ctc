/* 	-- tabs --
	Este codigo le da la funcionalidad a los tabs de navegacion
*/
var tabs = {
	//Esta function gestiona el cambio de un tab a otro
	changeState(e){
		//console.log("change state");
		var btn = e.target;
		var tab_domain = $(btn).attr('tab-domain');
		var tab_target = $(btn).attr('tab-target');
		var target = $("#"+tab_target);
		console.log("["+tab_domain+"]");
		if(!target.hasClass('active')){			
			$("[tab-domain='"+tab_domain+"']").removeClass('active');
			$(btn).addClass("active");
			$(target).addClass("active");
		}
	}
}
jQuery(document).ready(function($) {
	$(document).on('click', '.mc2-tab-buttons .btn', function(e) {
		event.preventDefault();
		tabs.changeState(e);
	});
});